import React, { createContext, useReducer } from 'react';
import PropTypes from 'prop-types';
import ContextDevTool from 'react-context-devtool';
import timeZoneReducer, { initialState } from '../reducers/timeZoneReducer';

export const TimeZoneContext = createContext();

const TimeZoneProvider = ({ children }) => {
  const useTimeZoneState = useReducer(timeZoneReducer, initialState);

  return (
    <TimeZoneContext.Provider value={[...useTimeZoneState]}>
      <ContextDevTool
        context={TimeZoneContext}
        id="TimeZoneContext"
        displayName="TimeZone Context"
      />
      {children}
    </TimeZoneContext.Provider>
  );
};

TimeZoneProvider.propTypes = {
  children: PropTypes.element.isRequired,
};

export default TimeZoneProvider;
