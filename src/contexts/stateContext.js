import React, { createContext, useReducer } from 'react';
import PropTypes from 'prop-types';
import ContextDevTool from 'react-context-devtool';
import statesReducer, { initialState } from '../reducers/statesReducer';

export const StatesContext = createContext();

const StatesProvider = ({ children }) => {
  const useStatesState = useReducer(statesReducer, initialState);

  return (
    <StatesContext.Provider value={[...useStatesState]}>
      <ContextDevTool context={StatesContext} id="StatesContext" displayName="States Context" />
      {children}
    </StatesContext.Provider>
  );
};

StatesProvider.propTypes = {
  children: PropTypes.element.isRequired,
};

export default StatesProvider;
