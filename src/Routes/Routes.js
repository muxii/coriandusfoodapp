import React, { Suspense, lazy } from "react";
import { HashRouter, Route, Switch } from "react-router-dom";
import Loader from "../utils/Loader";

const LoginPage = lazy(() => import("../Screens/Login/Login"));
const SignupPage = lazy(() => import("../Screens/SignUp/Signup"));
const orderDetails = lazy(() =>
  import("../Screens/Main/OrderDetails/orderDetails")
);

const ResetPasswordPage = lazy(() =>
  import("../Screens/ResetPassword/ResetPassword")
);
const ExploreMenu = lazy(() =>
  import("../Screens/Main/ExploreMenu/ExploreMenu")
);
const OrderLists = lazy(() => import("../Screens/Main/Order/Order"));
const ViewCart = lazy(() => import("../Screens/Main/ViewCart/ViewCart"));
export const Routes = () => {
  return (
    <HashRouter>
      <Suspense fallback={<Loader />}>
        <Switch>
          <Route exact path="/login" component={LoginPage} />
          <Route exact path="/signup" component={SignupPage} />
          <Route exact path="/reset" component={ResetPasswordPage} />
          <Route exact path="/explore" component={ExploreMenu} />
          <Route exact path="/viewCart" component={ViewCart} />
          <Route exact path="/orderdetails" component={orderDetails} />
          <Route exact path="/orderLists" component={OrderLists} />
        </Switch>
      </Suspense>
    </HashRouter>
  );
  // upload doc , doc repo , aggree. doc repo.
};

export default Routes;
