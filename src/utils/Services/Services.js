// http://localhost:8080/eurekasdk/api/foodDeliveryUsers/
// https://yesbankuat.eurekamobilesdk.com/eurekasdk/api/foodDeliveryAddress/getAllTowerList
// http://localhost:8080/eurekasdk/api/foodDeliveryUsers/foodDeliveryCreateUser
// http://35.154.91.20/eurekasdk/api/foodDeliveryProducts/getFoodDeliveryMenuCatagoriesDTOList
// 35.154.91.20/eurekasdk/api/foodDeliveryProducts/getFoodDeliveryMenueByMenueId
// http://localhost:8080/eurekasdk/api/foodDeliveryCart/getFoodDeliveryCartByUserId
// http://localhost:8080/eurekasdk/api/foodDeliveryCart/getCartTtemsCount
// http://localhost:8080/eurekasdk/api/foodDeliveryCart/addItemsToCart
// http://localhost:8080/eurekasdk/api/foodDeliveryCart/updateQuantityToItemsofCart
// http://35.154.91.20/eurekasdk/api/foodDeliveryCart/removeItemFromCart
// http://localhost:8080/eurekasdk/api/foodDeliveryOrder/createUserOrder
// http://35.154.91.20/eurekasdk/api/foodDeliveryOrder/getOrderListByUserId
import axios from "axios";

const ENDPOINTS = {
  AUTHORIZATION: "RXVyZWtAQWRtIW46SnVwIVQwckAx",
  APPID: "RXVyZWthWWVzYmFuazpFdXJla0BZZXNiQG5r",
  BASE_URL: "http://35.154.91.20/eurekasdk/api/",
  Login: "foodDeliveryUserLogin",
  USERS: "foodDeliveryUsers/",
  DELIVERYADD: "foodDeliveryAddress/",
  DELIVERYPRODUCT: "foodDeliveryProducts/",
  RESET: "foodDeliveryChangePassword",
  CREATE: "foodDeliveryCreateUser",
  GETTOWER: "getAllTowerList",
  GETFLAT: "getFlatListByTowerId",
  FOODDELIVERYCART: "foodDeliveryCart/",
  MENUBYMENUID: "getFoodDeliveryMenueByMenueId",
  FOODDELIVERYMENU: "getFoodDeliveryMenuCatagoriesDTOList",
  FOODDELIVERYCARTBYUSERID: "getFoodDeliveryCartByUserId",
  GETCARTITEMCOUNT: "getCartTtemsCount",
  ADDITEMSTOCART: "addItemsToCart",
  UPDATEQUANTITY: "updateQuantityToItemsofCart",
  REMOVEQUANTITY: "removeItemFromCart",
  CREATEUSERORDER: "createUserOrder",
  FOODDELIVERYORDER: "foodDeliveryOrder/",
  ORDERLISTBYUSERID: "getOrderListByUserId",
};

export const LoginService = (body) => {
  return axios.post(
    ENDPOINTS.BASE_URL + ENDPOINTS.USERS + ENDPOINTS.Login,
    body,

    {
      timeout: 3 * 60 * 1000,
      headers: {
        Authorization: ENDPOINTS.AUTHORIZATION,
        app_id: ENDPOINTS.APPID,
        "Content-Type": "application/json",
      },
    }
  );
};

export const ChangePasswordService = (body) => {
  return axios.post(
    ENDPOINTS.BASE_URL + ENDPOINTS.USERS + ENDPOINTS.RESET,
    body,
    {
      timeout: 3 * 60 * 1000,
      headers: {
        Authorization: ENDPOINTS.AUTHORIZATION,
        app_id: ENDPOINTS.APPID,
        "Content-Type": "application/json",
      },
    }
  );
};
export const UserCreationService = (body) => {
  return axios.post(
    ENDPOINTS.BASE_URL + ENDPOINTS.USERS + ENDPOINTS.CREATE,
    body,
    {
      timeout: 3 * 60 * 1000,
      headers: {
        Authorization: ENDPOINTS.AUTHORIZATION,
        app_id: ENDPOINTS.APPID,
        "Content-Type": "application/json",
      },
    }
  );
};

export const getTowerService = (body) => {
  return axios.post(
    ENDPOINTS.BASE_URL + ENDPOINTS.DELIVERYADD + ENDPOINTS.GETTOWER,
    body,
    {
      timeout: 3 * 60 * 1000,
      headers: {
        Authorization: ENDPOINTS.AUTHORIZATION,
        app_id: ENDPOINTS.APPID,
        "Content-Type": "application/json",
      },
    }
  );
};
export const getFlatById = (body) => {
  return axios.post(
    ENDPOINTS.BASE_URL + ENDPOINTS.DELIVERYADD + ENDPOINTS.GETFLAT,
    body,
    {
      timeout: 3 * 60 * 1000,
      headers: {
        Authorization: ENDPOINTS.AUTHORIZATION,
        app_id: ENDPOINTS.APPID,
        "Content-Type": "application/json",
      },
    }
  );
};
export const getMenu = (body) => {
  return axios.post(
    ENDPOINTS.BASE_URL + ENDPOINTS.DELIVERYPRODUCT + ENDPOINTS.FOODDELIVERYMENU,
    body,
    {
      timeout: 3 * 60 * 1000,
      headers: {
        Authorization: ENDPOINTS.AUTHORIZATION,
        app_id: ENDPOINTS.APPID,
        "Content-Type": "application/json",
      },
    }
  );
};
export const getMenuById = (body) => {
  return axios.post(
    ENDPOINTS.BASE_URL + ENDPOINTS.DELIVERYPRODUCT + ENDPOINTS.MENUBYMENUID,
    body,
    {
      timeout: 3 * 60 * 1000,
      headers: {
        Authorization: ENDPOINTS.AUTHORIZATION,
        app_id: ENDPOINTS.APPID,
        "Content-Type": "application/json",
      },
    }
  );
};
export const getCartById = (body) => {
  return axios.post(
    ENDPOINTS.BASE_URL +
      ENDPOINTS.FOODDELIVERYCART +
      ENDPOINTS.FOODDELIVERYCARTBYUSERID,
    body,
    {
      timeout: 3 * 60 * 1000,
      headers: {
        Authorization: ENDPOINTS.AUTHORIZATION,
        app_id: ENDPOINTS.APPID,
        "Content-Type": "application/json",
      },
    }
  );
};

export const getCartItemCount = (body) => {
  return axios.post(
    ENDPOINTS.BASE_URL +
      ENDPOINTS.FOODDELIVERYCART +
      ENDPOINTS.GETCARTITEMCOUNT,
    body,
    {
      timeout: 3 * 60 * 1000,
      headers: {
        Authorization: ENDPOINTS.AUTHORIZATION,
        app_id: ENDPOINTS.APPID,
        "Content-Type": "application/json",
      },
    }
  );
};

export const addCartItem = (body) => {
  return axios.post(
    ENDPOINTS.BASE_URL + ENDPOINTS.FOODDELIVERYCART + ENDPOINTS.ADDITEMSTOCART,
    body,
    {
      timeout: 3 * 60 * 1000,
      headers: {
        Authorization: ENDPOINTS.AUTHORIZATION,
        app_id: ENDPOINTS.APPID,
        "Content-Type": "application/json",
      },
    }
  );
};

export const addQuantity = (body) => {
  return axios.post(
    ENDPOINTS.BASE_URL + ENDPOINTS.FOODDELIVERYCART + ENDPOINTS.UPDATEQUANTITY,
    body,
    {
      timeout: 3 * 60 * 1000,
      headers: {
        Authorization: ENDPOINTS.AUTHORIZATION,
        app_id: ENDPOINTS.APPID,
        "Content-Type": "application/json",
      },
    }
  );
};

export const deleteQuantity = (body) => {
  return axios.post(
    ENDPOINTS.BASE_URL + ENDPOINTS.FOODDELIVERYCART + ENDPOINTS.REMOVEQUANTITY,
    body,
    {
      timeout: 3 * 60 * 1000,
      headers: {
        Authorization: ENDPOINTS.AUTHORIZATION,
        app_id: ENDPOINTS.APPID,
        "Content-Type": "application/json",
      },
    }
  );
};

export const CreateUserOrder = (body) => {
  return axios.post(
    ENDPOINTS.BASE_URL +
      ENDPOINTS.FOODDELIVERYORDER +
      ENDPOINTS.CREATEUSERORDER,
    body,
    {
      timeout: 3 * 60 * 1000,
      headers: {
        Authorization: ENDPOINTS.AUTHORIZATION,
        app_id: ENDPOINTS.APPID,
        "Content-Type": "application/json",
      },
    }
  );
};

export const UserOrderList = (body) => {
  return axios.post(
    ENDPOINTS.BASE_URL +
      ENDPOINTS.FOODDELIVERYORDER +
      ENDPOINTS.ORDERLISTBYUSERID,
    body,
    {
      timeout: 3 * 60 * 1000,
      headers: {
        Authorization: ENDPOINTS.AUTHORIZATION,
        app_id: ENDPOINTS.APPID,
        "Content-Type": "application/json",
      },
    }
  );
};
// removeItemFromCart
