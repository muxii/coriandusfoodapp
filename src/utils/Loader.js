import { Grid } from "@material-ui/core";
import React from "react";
import Image from "../Assets/delivery.gif";
import "./loader.css";
const Loader = () => {
  return (
    <div className="outerDiv">
      <Grid
        container
        xs={12}
        direction="row"
        justify="center"
        alignItems="center"
      >
        <img src={Image} alt="#delivery" className="loader" />
      </Grid>
    </div>
  );
};

export default Loader;
