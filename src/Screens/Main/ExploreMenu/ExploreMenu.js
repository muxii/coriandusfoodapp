import { Button, Grid } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import ScrollableTabsButtonPrevent from "../../../Components/Menu/Menu";
import {
  getCartById,
  getCartItemCount,
  getMenu,
  getMenuById,
  addCartItem,
} from "../../../utils/Services/Services";
import "./ExploreMenu.css";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import TemporaryDrawer from "../../../Components/Appbar/Appbar/Appbar";
import AppBar1 from "../../../Components/Appbar/Appbar/Appbar";
import Cookies from "js-cookie";
import { useHistory } from "react-router-dom";
import { LoginContext } from "../../../contexts/loginContext";
import { APPBAR_EVENTS } from "../../../constants/actionTypes";
import SnackbarComponent from "../../../Components/Snackbar/Snackbar";

const ExploreMenu = () => {
  const [menuData, setMenuData] = useState("");
  const [value, setValue] = React.useState("");
  const [menuItem, setMenuItem] = useState("");
  const [menuItemCount, setMenuItemCount] = useState(0);
  const [opensnack, setOpenSnack] = useState(false);
  const [message, setMessage] = useState("");
  const [type, setType] = useState("");
  const [showCart, setShowCart] = useState(false);
  const [loginState, loginDispatch] = React.useContext(LoginContext);

  const [pizzaDrop, setPizzaDrop] = useState("");
  const history = useHistory();

  useEffect(() => {
    console.log(loginState);
    const cookId = Cookies.get("UserId");
    getCartById({ userId: cookId })
      .then((cart) => {
        // console.log(cart);
        Cookies.set("cartId", cart.data.cartId);
      })

      .catch((err) => console.log(err));

    loginDispatch({
      type: APPBAR_EVENTS.SUCCESS,
      payload: "Hello how are you??",
    });
  }, []);

  useEffect(() => {
    const cookId = Cookies.get("UserId");
    const cartId = Cookies.get("cartId");
    getCartItemCount({ userId: cookId, cartId: cartId }).then((countres) => {
      //   console.log(countres);
      setMenuItemCount(countres.data.count);
      if (parseInt(countres.data.count) > 0) {
        setShowCart(true);
      } else {
        setShowCart(false);
      }
    });
  }, []);

  useEffect(() => {
    const cook = Cookies.get("login");
    if (!cook) {
      history.push("/login");
    }
    setMenuItem("");
    setPizzaDrop("");
    getMenu()
      .then((menuresult) => {
        console.log(menuresult.data.foodDeliveryMenueDTOList);
        if (menuresult.data)
          setMenuData(menuresult.data.foodDeliveryMenueDTOList);

        if (value == "") {
          setValue(menuresult.data.foodDeliveryMenueDTOList[0].menueId);
          getMenuById({
            menueId: menuresult.data.foodDeliveryMenueDTOList[0].menueId,
          })
            .then((menuresult) => {
              setMenuItem(
                menuresult.data.foodDeliveryMenueItemsDTOList
                  .foodDeliveryMenueItemsDTOList
              );
              setPizzaDrop(
                menuresult.data.foodDeliveryMenuItemsVarientCatagoryDTOList
                  .foodDeliveryMenuItemsVarientCatagoryDTOList
              );
            })
            .catch((err) => console.log(err));
        } else {
          getMenuById({
            menueId: value,
          })
            .then((menuresult) => {
              setMenuItem(
                menuresult.data.foodDeliveryMenueItemsDTOList
                  .foodDeliveryMenueItemsDTOList
              );
              setPizzaDrop(
                menuresult.data.foodDeliveryMenuItemsVarientCatagoryDTOList
                  .foodDeliveryMenuItemsVarientCatagoryDTOList
              );
            })
            .catch((err) => console.log(err));
        }

        //   console.log(menuresult);
      })
      .catch((err) => console.log(err));
  }, [value]);

  const showAddButton = (id) => {
    const cookId = Cookies.get("UserId");
    const cartId = Cookies.get("cartId");
    // console.log(id);
    addCartItem({
      userId: cookId,
      cartId: cartId,
      foodDeliveryCartAddedItemsDTO: {
        menueItemsId: id,
      },
    }).then((cartItemres) => {
      console.log(cartItemres);
      if (cartItemres.data) {
        getCartItemCount({ userId: cookId, cartId: cartId }).then(
          (countres) => {
            setMenuItemCount(countres.data.count);
            setOpenSnack(!opensnack);
            setMessage(`${countres.data.count} Item added to Cart`);
            setType("success");

            setTimeout(() => {
              setOpenSnack(false);
            }, 1500);
          }
        );
        setShowCart(true);
      }
    });
  };

  return (
    <AppBar1 menuItemCount={menuItemCount && menuItemCount}>
      {opensnack && (
        <SnackbarComponent
          autoHideDuration={1500}
          open={opensnack}
          onClose={() => setOpenSnack(false)}
          isSuccess={type}
          style={{ marginBottom: "80px" }}
          handleClose={() => setOpenSnack(false)}
          message={message}
        />
      )}
      <Grid
        container
        style={{
          height: "100vh",
          backgroundColor: "black",
          borderBottomColor: "2px solid gray",
        }}
      >
        {/* <div className="inputelement"> */}
        <Grid
          item
          xs={12}
          md={12}
          style={{ borderBottomColor: "2px solid gray" }}
        >
          <div
            style={{
              marginTop: "50px",
              marginBottom: "50px",
              borderBottomColor: "2px solid gray",
            }}
          >
            <ScrollableTabsButtonPrevent
              setValue={setValue}
              menuItem={menuItem}
              menu={menuData}
              value={value}
              pizzaDrop={pizzaDrop}
              setShowCart={setShowCart}
              showAddButton={showAddButton}
            />
          </div>
        </Grid>
        {showCart && (
          <Grid item xs={12} md={12} className="bottomMenuBar">
            <div className="cart-text">
              <div>Cart: {menuItemCount} Item</div>
              <div>
                <Button
                  color="primary"
                  variant="contained"
                  onClick={() => history.push("/viewCart")}
                >
                  View Cart
                </Button>
              </div>
            </div>
          </Grid>
        )}
      </Grid>
    </AppBar1>
  );
};

export default ExploreMenu;
