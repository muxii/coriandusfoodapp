import React, { useEffect, useState } from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import SkipPreviousIcon from "@material-ui/icons/SkipPrevious";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import SkipNextIcon from "@material-ui/icons/SkipNext";
import { Button, ButtonGroup, Grid } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import KeyboardBackspaceIcon from "@material-ui/icons/KeyboardBackspace";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import RemoveIcon from "@material-ui/icons/Remove";
import {
  addCartItem,
  getCartById,
  addQuantity,
  deleteQuantity,
} from "../../../utils/Services/Services";
import Cookies from "js-cookie";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";
import Image from "../../../Assets/emptycart.gif";
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    padding: "10px",
    margin: "10px",
    width: "88%",
    height: 120,
  },
  details: {
    display: "flex",
    // flexDirection: "column",
    // paddingTop: "45px",
  },

  content: {
    paddingLeft: 15,
    paddingRight: 0,
    paddingTop: 0,
    paddingBottom: 0,
    // flex: "1 0 auto",
  },
  cover: {
    width: 50,
    height: 50,
  },
  controls: {
    display: "flex",
    alignItems: "center",
    paddingLeft: theme.spacing(1),
    paddingBottom: theme.spacing(1),
  },
  playIcon: {
    height: 38,
    width: 38,
  },
}));

const ViewCart = () => {
  const [cartItems, setCartItems] = useState("");
  const [quantity, setQuantity] = useState(0);
  const [amount, setAmount] = useState(0);

  const [disabled, setDisabled] = useState(false);
  const classes = useStyles();
  const theme = useTheme();
  const history = useHistory();

  useEffect(() => {
    const cookId = Cookies.get("UserId");

    getCartById({ userId: cookId }).then((res) => {
      setAmount(res.data.totalAmount);
      setCartItems(res.data.foodDeliveryCartAddedItemsDTOList);
    });
  }, [amount]);

  const DeleteFn = (id, quant) => {
    const cookId = Cookies.get("UserId");
    const cartId = Cookies.get("cartId");
    setDisabled(true);

    deleteQuantity({
      userId: cookId,
      cartId: cartId,
      deleteCartItem: id,
    }).then((deleteresponse) => {
      if (deleteresponse.data) {
        getCartById({ userId: cookId }).then((res) => {
          if (res.data) {
            setDisabled(false);
            setAmount(res.data.totalAmount);
            setCartItems(res.data.foodDeliveryCartAddedItemsDTOList);
          } else {
            setDisabled(true);
          }
        });
      }
    });
  };
  const reduceFn = (id, quant) => {
    const cookId = Cookies.get("UserId");
    const cartId = Cookies.get("cartId");
    setQuantity(quant - 1);
    setDisabled(true);
    addQuantity({
      userId: cookId,
      cartId,
      cartItemId: id,
      updatedQuantity: quant != null ? +quant + -1 : "" + (quantity - 1),
    }).then((addresponse) => {
      if (addresponse.data) {
        getCartById({ userId: cookId }).then((res) => {
          if (res.data) {
            setDisabled(false);
            setAmount(res.data.totalAmount);
            setCartItems(res.data.foodDeliveryCartAddedItemsDTOList);
          } else {
            setDisabled(true);
          }
        });
      }
    });
  };

  const quantityFn = (id, quant) => {
    const cookId = Cookies.get("UserId");
    const cartId = Cookies.get("cartId");
    setQuantity(quant + 1);
    setDisabled(true);
    addQuantity({
      userId: cookId,
      cartId,
      cartItemId: id,
      updatedQuantity: quant != null ? +quant + +1 : "" + (quantity + 1),
    }).then((addresponse) => {
      if (addresponse.data) {
        getCartById({ userId: cookId })
          .then((res) => {
            if (res.data) {
              setDisabled(false);
              setAmount(res.data.totalAmount);
              setCartItems(res.data.foodDeliveryCartAddedItemsDTOList);
            } else {
              setDisabled(true);
            }
          })
          .catch((err) => console.log(err));
      }
    });
  };

  return (
    <Grid style={{ height: "100vh", backgroundColor: "black" }}>
      <Grid
        item
        xs={12}
        md={12}
        style={{
          padding: "8px",
          width: "100%",
          backgroundColor: "black",
          borderBottom: "2px solid gray",
          color: "white",
          position: "fixed",
          top: 0,
          zIndex: 1000,
        }}
        onClick={() => history.push("/explore")}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <ArrowBackIcon />
          <div
            style={{
              display: "flex",
              alignItems: "center",
              width: "100%",
              justifyContent: "space-between",
            }}
          >
            <div style={{ padding: "5px" }}>
              {cartItems && cartItems ? cartItems.itemCount : 0} Item in Cart
            </div>
            <div style={{ padding: "5px", paddingLeft: "30px" }}>
              You Pay : {amount ? amount : 0} &#x20B9;
            </div>
          </div>
        </div>
      </Grid>
      <Grid
        container
        style={{
          backgroundColor: "black",
          borderBottomColor: "2px solid gray",
          padding: "10px",
          paddingTop: "70px",
        }}
      >
        <Grid item xs={12} md={12}>
          {cartItems && cartItems != null ? (
            cartItems.map((response) => {
              return (
                <>
                  <Card className={classes.root}>
                    <CardMedia
                      className={classes.cover}
                      // image={response.imageUrl}
                      title={response.name}
                    >
                      <img
                        src={response.imageUrl}
                        style={{ width: "50px", height: "50px" }}
                      />
                    </CardMedia>

                    <div className={classes}>
                      <CardContent className={classes.content}>
                        <div>{response.name}</div>
                        {response.varientDTO &&
                          response.varientDTO.map((varresponse) => {
                            //   console.log(varresponse),
                            return (
                              <>
                                <Typography
                                  variant="subtitle1"
                                  style={{ fontSize: "12px" }}
                                  color="textSecondary"
                                >
                                  {varresponse.addOnPrice +
                                    "  " +
                                    varresponse.varientName +
                                    " "}
                                  &#x20B9;
                                </Typography>
                              </>
                            );
                          })}

                        <div>
                          <ButtonGroup
                            style={{
                              maxWidth: "15px",
                              maxHeight: "25px",
                              minWidth: "20px",
                            }}
                            color="primary"
                          >
                            {parseInt(response.quantity) > 1 ? (
                              <Button
                                disable={disabled}
                                onClick={() =>
                                  reduceFn(
                                    response.cartItemsId,
                                    response.quantity
                                  )
                                }
                              >
                                <RemoveIcon />
                              </Button>
                            ) : (
                              <Button
                                disable={disabled}
                                onClick={() =>
                                  DeleteFn(
                                    response.cartItemsId,
                                    response.quantity
                                  )
                                }
                              >
                                <DeleteIcon style={{ fontSize: "18px" }} />
                              </Button>
                            )}

                            <Button disabled={true} style={{ color: "black" }}>
                              {
                                response.quantity
                                //   response.foodDeliveryCartAddedItemsDTOList &&
                                //     response.foodDeliveryCartAddedItemsDTOList.map(
                                //       (quant) => {
                                //         console.log(response);
                                //         return quant.quantity;
                                //       }
                                //     ))
                              }
                            </Button>
                            <Button disable={disabled}>
                              <AddIcon
                                onClick={() =>
                                  quantityFn(
                                    response.cartItemsId,
                                    response.quantity
                                  )
                                }
                                style={{ fontSize: "18px" }}
                              />
                            </Button>
                          </ButtonGroup>
                        </div>
                      </CardContent>
                    </div>
                    <div
                      style={{
                        display: "flex",
                        alignItems: "flex-end",
                        width: "120px",
                        justifyContent: "flex-end",
                      }}
                      color="textSecondary"
                    >
                      <Typography component="h1">
                        &#x20B9; {response.totalAmount}
                      </Typography>
                    </div>
                  </Card>
                </>
              );
            })
          ) : (
            <>
              <Grid
                container
                xs={12}
                direction="row"
                justify="center"
                alignItems="center"
                style={{ height: "40vh" }}
              >
                <div>
                  <img
                    src={Image}
                    alt="#delivery"
                    style={{
                      width: "80px",
                      height: "80px",
                    }}
                  />
                </div>
              </Grid>
              <Typography
                component="h2"
                variant="h5"
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  color: "white",
                }}
              >
                Your Cart is Empty
              </Typography>
              <Typography
                component="h4"
                variant="small"
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  color: "white",
                }}
              >
                Add Items to Enjoy delicious food!
              </Typography>
            </>
          )}
        </Grid>
        {amount != null && (
          <Grid container style={{ padding: "10px", marginBottom: "50px" }}>
            <Card
              style={{
                display: "flex",
                width: "100%",
                padding: "10px",
              }}
            >
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  width: "100%",
                  justifyContent: "space-between",
                }}
              >
                <div>
                  <p>SubTotal</p>
                  <p>Discount</p>
                  <p>Taxes & Charges</p>
                  <p>
                    <hr style={{ border: "1px solid gray" }} />
                  </p>
                  <p>Grand Total</p>
                </div>
                <div>
                  <p>:</p>
                  <p>:</p>
                  <p>:</p>
                  <p>:</p>
                  <p>:</p>
                </div>
                <div>
                  <p>{amount ? amount : 0} &#x20B9;</p>
                  <p>00 &#x20B9;</p>
                  <p>00 &#x20B9;</p>

                  <p>
                    <hr style={{ border: "1px solid gray" }} />
                  </p>
                  <p>{amount} &#x20B9;</p>
                </div>
              </div>
            </Card>
          </Grid>
        )}
      </Grid>
      {amount != null && (
        <Grid
          item
          xs={12}
          md={12}
          style={{
            padding: "8px",
            width: "100%",
            backgroundColor: "black",
            borderTop: "2px solid gray",
            color: "white",
            position: "fixed",
            bottom: 0,
            zIndex: 1000,
          }}
        >
          <Button
            // color="primary"
            variant="contained"
            fullWidth
            style={{ backgroundColor: "rgb(66, 138, 33)", color: "white" }}
            onClick={() => history.push("/orderdetails")}
          >
            Check Out
          </Button>
        </Grid>
      )}
    </Grid>
  );
};

export default ViewCart;
