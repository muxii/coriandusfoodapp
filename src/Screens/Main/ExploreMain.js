import React from "react";
import AppBar1 from "../../Components/Appbar/Appbar/Appbar";

import ExploreMenu from "./ExploreMenu/ExploreMenu";

export default function Dashboard(props) {
  return (
    <div>
      <AppBar1 page={<ExploreMenu />} />
    </div>
  );
}
