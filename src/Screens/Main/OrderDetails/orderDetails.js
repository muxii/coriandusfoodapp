import {
  Card,
  Grid,
  CardMedia,
  FormControl,
  InputLabel,
  makeStyles,
  Select,
  MenuItem,
  TextField,
  Button,
  Typography,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import Cookies from "js-cookie";
import { CreateUserOrder, getCartById } from "../../../utils/Services/Services";
import { useHistory } from "react-router-dom";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import {
  KeyboardDatePicker,
  KeyboardTimePicker,
  MuiPickersUtilsProvider,
  TimePicker,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import moment from "moment";
import "./orderDetails.css";
const useStyles1 = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 220,
    color: "white",
  },
  select: {
    borderBottom: "3px solid white",
    borderRadius: "5%",
    color: "white",
    fontSize: "16px",
  },
  container: {
    display: "flex",
    flexWrap: "wrap",
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));
export const OrderDetails = () => {
  const classes1 = useStyles1();
  const [cartItems, setCartItems] = useState("");
  const [select, setSelect] = useState("");
  const [selectPayment, setSelectPayment] = useState("");
  const [cartData, setCartData] = useState("");
  const [file, setFile] = useState("");

  const [selectedDate, setSelectedDate] = useState(moment());
  const [inputTime, setInputTime] = useState(moment());
  const [selectTime, setSelectTime] = useState(moment().format("hh:mm:ss"));
  const [inputValue, setInputValue] = useState(moment().format("YYYY:MM:DD"));
  const history = useHistory();
  useEffect(() => {
    const newDate = new Date();
    console.log(newDate);
    const cookId = Cookies.get("UserId");
    getCartById({ userId: cookId })
      .then((res) => {
        if (res.data) {
          console.log(res.data.foodDeliveryUsersDTO.address);
          setCartData(res.data);
          setCartItems(res.data.foodDeliveryCartAddedItemsDTOList);
        } else {
        }
      })
      .catch((err) => console.log(err));
  }, []);

  const disableWeekends = (e) => {
    const date = new Date();
    // console.log(e);
    return e < date && e === date;
  };

  const SubmitOrder = () => {
    const cookId = Cookies.get("UserId");
    const userId = Cookies.get("cartId");
    const formdata = new FormData();
    if (userId && cookId && inputValue && inputTime && selectPayment) {
      if (select == "order Now") {
        formdata.append("cartId", userId);
        formdata.append("userId", cookId);
        formdata.append("deliveryDate", inputValue + ":" + inputTime);
        formdata.append("paymentType", selectPayment);
        formdata.append("paymentStatus", "Payment");
        formdata.append("adImage1File", file);

        CreateUserOrder(formdata)
          .then((formdataresponse) => {
            if (formdataresponse.status == 200) {
              history.push("/orderLists");
            }
          })
          .catch((err) => console.log(err));
      } else {
        var now = moment();
        var time = now.hour() + ":" + now.minutes() + ":" + "00";

        const date = moment(now).format("yyyy:MM:DD");
        formdata.append("cartId", userId);
        formdata.append("userId", cookId);
        formdata.append("deliveryDate", date + ":" + time);
        formdata.append("paymentType", selectPayment);
        formdata.append("paymentStatus", "Payment");
        CreateUserOrder(formdata)
          .then((formdataresponse) => {
            if (formdataresponse.status == 200) {
              history.push("/orderLists");
            }
          })
          .catch((err) => console.log(err));
      }
    } else {
    }
  };

  const handleDateChange = (e, value) => {
    const date = new Date();
    console.log(e);

    setSelectedDate(e);
    console.log(value);
    setInputValue(value);
  };
  const handleTimeChange = (e, value) => {
    console.log(e, value);
    const time = moment(e).format("HH:mm:00");
    console.log(time);
    setSelectTime(e);
    setInputTime(e);
  };

  const handleChangeselect = (e) => {
    console.log(e);
    setSelect(e.target.value);
  };

  const handleChangeselectPayment = (e) => {
    setSelectPayment(e.target.value);
  };
  const fileChange = (e) => {
    const filetypes = /jpeg|jpg|png|webp/;
    console.log(e.target.files[0]);
    if (e.target.files[0]) {
      if (e.target.files[0].size < 1000000) {
        setFile(e.target.files[0]);
      }
    }
  };
  return (
    <Grid style={{ height: "100vh", backgroundColor: "black" }}>
      <Grid
        item
        xs={12}
        md={12}
        style={{
          padding: "8px",
          width: "100%",
          backgroundColor: "black",
          borderBottom: "2px solid gray",
          color: "white",
          position: "fixed",
          top: 0,
          zIndex: 1000,
        }}
        onClick={() => history.push("/viewCart")}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <ArrowBackIcon />
          <div
            style={{
              display: "flex",
              alignItems: "center",
              width: "100%",
              justifyContent: "center",
            }}
          >
            <div style={{ padding: "5px", fontSize: "18px" }}>
              Place Order
              {/* {cartItems && cartItems ? cartItems.itemCount : 0} Item in Cart */}
            </div>
            {/* <div style={{ padding: "5px", paddingLeft: "30px" }}>
              You Pay : {amount ? amount : 0} &#x20B9;
            </div> */}
          </div>
        </div>
      </Grid>

      <Grid item xs={12} md={12}>
        <div
          style={{
            display: "flex",
            marginTop: "40px",
            justifyContent: "center",
            alignItems: "center",
            padding: "20px",
            marginBottom: "100px",
          }}
        >
          <Card
            style={{
              width: "100%",
              padding: "10px",
            }}
          >
            <CardMedia>
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div>
                  <p>Item Count</p>
                  <p>Total Amount</p>
                  <p>Name</p>
                  <p>Address</p>
                </div>
                <div>
                  <p>{cartData.itemCount}</p>
                  <p>{cartData.totalAmount}</p>
                  <p>{cartData.userName}</p>
                  <p>{cartData && cartData.foodDeliveryUsersDTO.address}</p>
                </div>
              </div>
              <FormControl className={classes1.formControl}>
                <InputLabel
                  id="demo-simple-select-label"
                  style={{ color: "black" }}
                >
                  Order Time
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  defaultValue={select}
                  fullWidth
                  onChange={(e) => handleChangeselect(e)}
                  className={classes1.select}
                  style={{ color: "black" }}
                >
                  <MenuItem style={{ fontSize: "18px" }} value="order now">
                    order now
                  </MenuItem>
                  <MenuItem style={{ fontSize: "18px" }} value="select time">
                    select Date/time
                  </MenuItem>
                </Select>
              </FormControl>
              {select == "select time" && (
                <>
                  <Grid item xs={12} md={12}>
                    <MuiPickersUtilsProvider
                      className={classes1.formControl}
                      utils={DateFnsUtils}
                    >
                      <KeyboardDatePicker
                        margin="normal"
                        id="date-picker-dialog"
                        label="Date"
                        format="yyyy:MM:dd"
                        inputValue={inputValue}
                        minDate={selectedDate}
                        shouldDisableDate={disableWeekends}
                        value={selectedDate}
                        onChange={handleDateChange}
                        KeyboardButtonProps={{
                          "aria-label": "change date",
                        }}
                      />
                    </MuiPickersUtilsProvider>
                  </Grid>
                  <Grid item xs={12} md={12}>
                    <MuiPickersUtilsProvider
                      className={classes1.formControl}
                      utils={DateFnsUtils}
                    >
                      <TimePicker
                        clearable
                        ampm={false}
                        label="Select Time"
                        value={inputTime}
                        inputValue={selectTime}
                        onChange={(e) => handleTimeChange(e)}
                      />
                    </MuiPickersUtilsProvider>
                  </Grid>
                </>
              )}
              <FormControl className={classes1.formControl}>
                <InputLabel
                  id="demo-simple-select-label"
                  style={{ color: "black" }}
                >
                  Payment Type
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  defaultValue={selectPayment}
                  fullWidth
                  onChange={(e) => handleChangeselectPayment(e)}
                  className={classes1.select}
                  style={{ color: "black" }}
                >
                  <MenuItem
                    style={{ fontSize: "18px" }}
                    value="Cash on Delivery"
                  >
                    Cash on Delivery
                  </MenuItem>
                  <MenuItem style={{ fontSize: "18px" }} value="Paytm">
                    Paytm
                  </MenuItem>
                </Select>
              </FormControl>
              {selectPayment == "Paytm" ? (
                <Grid container>
                  <Grid xs={12} md={12} lg={12}>
                    <div>
                      <div
                        style={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <Typography>Please Pay the Amount on </Typography>
                      </div>
                      <div
                        style={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <Typography variant="h5" color="green">
                          Paytm No. 9811800551
                        </Typography>
                      </div>
                      <div
                        style={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <Typography>
                          Please Take the Screen Shot and Upload
                        </Typography>
                      </div>
                      <div
                        style={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        <Typography>payment Confirmation Image</Typography>
                      </div>
                    </div>
                  </Grid>
                  <Grid container>
                    <Grid xs={12} md={12} lg={12}>
                      <div
                        style={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                          margin: "20px",
                        }}
                      >
                        <input
                          type="file"
                          accept="image/*"
                          onChange={(e) => fileChange(e)}
                          //   onClick={fileChange}
                        />
                      </div>
                    </Grid>
                  </Grid>
                </Grid>
              ) : (
                ""
              )}
            </CardMedia>
          </Card>
        </div>
      </Grid>
      <Grid
        item
        xs={12}
        md={12}
        style={{
          padding: "8px",
          width: "100%",
          backgroundColor: "black",
          borderTop: "2px solid gray",
          color: "white",
          position: "fixed",
          bottom: 0,
          zIndex: 1000,
        }}
      >
        <Button
          // color="primary"
          variant="contained"
          fullWidth
          style={{ backgroundColor: "rgb(66, 138, 33)", color: "white" }}
          onClick={() => SubmitOrder()}
        >
          Place Order
        </Button>
      </Grid>
    </Grid>
  );
};

export default OrderDetails;
