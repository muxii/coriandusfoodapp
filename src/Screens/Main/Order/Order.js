import React, { useEffect, useState } from "react";
import { Card, CardMedia, Grid } from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { UserOrderList } from "../../../utils/Services/Services";
import { useHistory } from "react-router-dom";
import Cookies from "js-cookie";
import HomeIcon from "@material-ui/icons/Home";
const OrderPage = () => {
  const [list, setList] = useState("");
  const history = useHistory();

  useEffect(() => {
    const userId = Cookies.get("UserId");
    UserOrderList({ userId, date: "07/01/2021" }).then((result) => {
      //   console.log(result);
      setList(result.data.foodDeliveryOrderDTOList);
    });
  }, []);

  return (
    <Grid container style={{ height: "100vh", backgroundColor: "black" }}>
      <Grid
        item
        xs={12}
        md={12}
        style={{
          padding: "8px",
          width: "100%",
          backgroundColor: "black",
          borderBottom: "2px solid gray",
          color: "white",
          position: "fixed",
          top: 0,
          zIndex: 1000,
        }}
        onClick={() => history.push("/explore")}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <HomeIcon />

          <div
            style={{
              display: "flex",
              alignItems: "center",
              width: "100%",
              justifyContent: "center",
            }}
          >
            <div style={{ padding: "5px", fontSize: "18px" }}>
              Order Details
              {/* {cartItems && cartItems ? cartItems.itemCount : 0} Item in Cart */}
            </div>
            {/* <div style={{ padding: "5px", paddingLeft: "30px" }}>
              You Pay : {amount ? amount : 0} &#x20B9;
            </div> */}
          </div>
        </div>
      </Grid>

      <Grid item sm={12} xs={12} md={12} lg={12}>
        <div style={{ marginTop: "40px", backgroundColor: "black" }}>
          {list &&
            list.map((res) => {
              return (
                <>
                  <div
                    style={{
                      display: "flex",
                      padding: "20px",
                    }}
                  >
                    <div
                      style={{
                        width: "100%",
                        padding: "10px",
                        backgroundColor: "white",
                      }}
                    >
                      <div
                        style={{
                          padding: "5px",
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                        }}
                      >
                        {res.createdDate}
                      </div>
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                          padding: "5px",
                        }}
                      >
                        <div>Order Id: {res.orderId}</div>
                        <div>
                          Amount Paid: {res.totalAmount ? res.totalAmount : ""}{" "}
                          &#x20B9;
                        </div>
                      </div>
                      <div style={{ padding: "5px" }}>
                        Order Status: {res.orderStatus}
                      </div>
                      <div style={{ padding: "5px" }}>
                        Payment Status: {res.paymentStatus}
                      </div>
                      {res &&
                        res.foodDeliveryOrderAddedItemsDTOList.map(
                          (finalres) => {
                            //   console.log(finalres);
                            return (
                              <>
                                <div
                                  style={{
                                    display: "flex",

                                    width: "100%",
                                  }}
                                >
                                  <div
                                    style={{
                                      display: "flex",
                                      // alignContent: "center",
                                      margin: "5px",
                                      justifyContent: "flex-start",
                                      width: "20%",
                                    }}
                                  >
                                    <img
                                      src={finalres.imageUrl}
                                      style={{
                                        width: "50px",
                                        margin: "5px",
                                        height: "50px",
                                      }}
                                    />
                                  </div>
                                  <div
                                    style={{
                                      display: "flex",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      width: "60%",
                                    }}
                                  >
                                    {finalres.name}
                                  </div>
                                  <div
                                    style={{
                                      display: "flex",
                                      alignContent: "center",
                                      justifyContent: "flex-end",
                                      float: "right",
                                      width: "20%",
                                    }}
                                  >
                                    {finalres.totalAmount} &#x20B9;
                                  </div>
                                </div>
                              </>
                            );
                          }
                        )}
                    </div>
                  </div>
                </>
              );
            })}
        </div>
      </Grid>
    </Grid>
  );
};

export default OrderPage;
