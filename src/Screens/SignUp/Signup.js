import {
  Button,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@material-ui/core";
import React, { useState, useEffect } from "react";
import Loader from "../../utils/Loader";
import Image from "../../Assets/logo.jpeg";
import "./Signup.css";
import {
  LoginService,
  getTowerService,
  getFlatById,
  UserCreationService,
} from "../../utils/Services/Services";
import { Redirect, useHistory } from "react-router-dom";
import SnackbarComponent from "../../Components/Snackbar/Snackbar";

const Signup = () => {
  const [Name, setName] = useState("");
  const [lName, setLname] = useState("");
  const [Password, setPassword] = useState("");
  const [tower, setTower] = useState("");
  const [opensnack, setOpenSnack] = useState(false);
  const [message, setMessage] = useState("");
  const [type, setType] = useState("");
  const [towerValue, setTowerValue] = useState("");
  const [towerId, setTowerId] = useState(0);
  const [flat, setFlat] = useState("");
  const [flatName, setFlatName] = useState("");
  const [flatId, setFlatId] = useState(0);
  const [address, setAddress] = useState(towerValue + flatName);
  const [mobile, setMobile] = useState(0);
  const [gender, setGender] = useState("");
  const [extention, setExtention] = useState(0);
  const [email, setEmail] = useState("");
  const history = useHistory();

  useEffect(() => {
    getTowerService()
      .then((response) => {
        setTower(response.data.foodDeliveryTowerDTO);
      })
      .catch((err) => {
        console.log(err);
      });

    getFlatById({ towerId: towerId })
      .then((flatResponse) => {
        setFlat(flatResponse.data.foodDeliveryFlatNoDTO);
      })
      .catch((err) => console.log(err));
  }, [towerId]);

  const onSubmit = () => {
    UserCreationService({
      userName: towerValue + flatName,
      userPassword: Password,
      firstName: Name,
      lastName: lName,
      mobileNumber: mobile,
      gender: gender,
      extenssionNo: extention,
      emailId: email,
      address: towerValue + "-" + flatName + " " + "INDIABULLS CENTRUM PARK",
      foodDeliveryTowerId: towerId,
      foodDeliveryFlatNoId: flatId,
    })
      .then((response) => {
        console.log(response);
        if (response.status == 201) {
          setOpenSnack(true);
          setType("success");
          setMessage(
            "Successfully Registered, Please Sign in to enjoy the food"
          );
          setTimeout(() => {
            setOpenSnack(false);
            history.push("/login");
          }, 4000);
        } else {
          setOpenSnack(true);
          setType("fail");
          setMessage("Fail to Register");
          setTimeout(() => {
            setOpenSnack(false);
          }, 4000);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleFlat = (e) => {
    console.log(e.target.value);
    setFlatName(e.target.value);

    if (e.target.value) {
      const flatId = flat
        .filter(
          (currentval) =>
            // console.log(currentval.towerName == e.target.value)
            currentval.flatNo == e.target.value
        )
        .map((filterelement) => {
          return filterelement.flatNoId;
        });
      setFlatId(flatId[0]);
    }
  };

  const handleChange = (e) => {
    console.log(e.target.value);
    setTowerValue(e.target.value);

    if (e.target.value) {
      const towerId = tower
        .filter(
          (currentval) =>
            // console.log(currentval.towerName == e.target.value)
            currentval.towerName == e.target.value
        )
        .map((filterelement) => {
          return filterelement.towerId;
        });
      console.log(towerId[0]);
      setTowerId(towerId[0]);
    }
  };
  console.log(tower);
  return (
    <>
      {opensnack && (
        <SnackbarComponent
          autoHideDuration={3000}
          open={opensnack}
          onClose={() => setOpenSnack(false)}
          isSuccess={type}
          handleClose={() => setOpenSnack(false)}
          message={message}
        />
      )}
      {
        <div
          className="outerContainer"
          style={{ padding: "10px", backgroundColor: "black" }}
        >
          <div className="Logintext" style={{ backgroundColor: "black" }}>
            <div
              className=""
              style={{
                backgroundColor: "black",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <img src={Image} style={{ height: "150px" }} />
            </div>
          </div>

          <div className="logincont1">
            <div
              className=""
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <h3 style={{ color: "rgb(66, 138, 33)" }}>
                Kindly Sinup to Continue
              </h3>
            </div>
            <Grid container spacing={0}>
              {/* <div className="inputelement"> */}
              <Grid item xs={12} md={12} className="select">
                <InputLabel id="demo-simple-select-label">Tower</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  fullWidth
                  label="Gender"
                  value={towerValue}
                  // value={age}
                  onChange={(e) => handleChange(e)}
                >
                  {tower &&
                    tower.map((towerdata) => {
                      return (
                        <MenuItem
                          key={towerdata.towerId}
                          value={towerdata.towerName}
                        >
                          {towerdata.towerName}
                        </MenuItem>
                      );
                    })}
                </Select>
              </Grid>
              <Grid item xs={12} md={12} className="select">
                <InputLabel id="demo-simple-select-label">Flat</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  fullWidth
                  label="Gender"
                  value={flatName}
                  // value={age}
                  onChange={(e) => handleFlat(e)}
                >
                  {flat &&
                    flat.map((towerdata) => {
                      return (
                        <MenuItem
                          key={towerdata.flatNoId}
                          value={towerdata.flatNo}
                        >
                          {towerdata.flatNo}
                        </MenuItem>
                      );
                    })}
                </Select>
              </Grid>
              <Grid item xs={12} md={12} className="inputgrid1">
                <TextField
                  id="standard-basic"
                  className="textfield"
                  label="Address"
                  fullWidth
                  value={
                    tower !== "" && flatName !== ""
                      ? towerValue +
                        "-" +
                        flatName +
                        "" +
                        "INDIABULLS CENTRUM PARK"
                      : ""
                  }
                  // onChange={(e) => setAddress()}
                  style={{ color: "white" }}
                />
              </Grid>
              <Grid item xs={12} md={12} className="inputgrid1">
                <TextField
                  id="standard-basic"
                  label="Password"
                  fullWidth
                  value={Password}
                  onChange={(e) => setPassword(e.target.value)}
                  style={{ color: "white" }}
                />
              </Grid>
              <Grid item xs={12} md={12} className="inputgrid1">
                <TextField
                  id="standard-basic"
                  label="First Name"
                  fullWidth
                  value={Name}
                  onChange={(e) => setName(e.target.value)}
                  style={{ color: "white" }}
                />
              </Grid>
              <Grid item xs={12} md={12} className="inputgrid1">
                <TextField
                  id="standard-basic"
                  label="Last Name"
                  fullWidth
                  value={lName}
                  onChange={(e) => setLname(e.target.value)}
                  style={{ color: "white" }}
                />
              </Grid>
              <Grid item xs={12} md={12} className="inputgrid1">
                <TextField
                  id="standard-basic"
                  label="Mobile Number"
                  fullWidth
                  value={mobile}
                  onChange={(e) => setMobile(e.target.value)}
                  style={{ color: "white" }}
                />
              </Grid>

              <Grid item xs={12} md={12} className="select">
                <InputLabel id="demo-simple-select-label">Gender</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  fullWidth
                  label="Gender"
                  value={gender}
                  onChange={(e) => setGender(e.target.value)}
                >
                  <MenuItem value="Male">Male</MenuItem>
                  <MenuItem value="Female">Female</MenuItem>
                  <MenuItem value="Others">Others</MenuItem>
                </Select>
              </Grid>
              <Grid item xs={12} md={12} className="inputgrid1">
                <TextField
                  id="standard-basic"
                  label="Extention Number"
                  fullWidth
                  value={extention}
                  onChange={(e) => setExtention(e.target.value)}
                  style={{ color: "white" }}
                />
              </Grid>

              <Grid item xs={12} md={12} className="inputgrid1">
                <TextField
                  id="standard-basic"
                  label="Email Id"
                  fullWidth
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  style={{ color: "white" }}
                />
              </Grid>
              <Grid item xs={12} md={12} className="inputgrid1">
                <Button
                  variant="contained"
                  fullWidth
                  onClick={onSubmit}
                  className="buttoncolor"
                >
                  SignUp
                </Button>
              </Grid>
              <Grid item xs={12} md={12} className="inputgrid1">
                <Button
                  variant="contained"
                  fullWidth
                  onClick={() => history.push("/login")}
                  className=""
                  color="dark"
                >
                  Cancel
                </Button>
              </Grid>
              {/* </div> */}
            </Grid>
          </div>
        </div>
      }
    </>
  );
};

export default Signup;
