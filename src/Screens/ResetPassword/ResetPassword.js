import {
  Button,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@material-ui/core";
import React, { useState } from "react";
import Loader from "../../utils/Loader";
import Image from "../../Assets/logo.jpeg";
import "./ResetPassword.css";
import { LoginService } from "../../utils/Services/Services";
import { useHistory } from "react-router-dom";

const Login = () => {
  const [Name, setName] = useState("");
  const [Password, setPassword] = useState("");

  const history = useHistory();

  const onSubmit = () => {
    LoginService({ userName: Name, userPassword: Password })
      .then((response) => {
        console.log(response);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  console.log(Name, Password);
  return (
    <div
      className="outerContainer"
      style={{ height: "100vh", backgroundColor: "black" }}
    >
      <div className="Logintext" style={{ backgroundColor: "black" }}>
        <div
          className=""
          style={{
            backgroundColor: "black",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <img src={Image} style={{ height: "150px" }} />
        </div>
      </div>

      <div className="logincont">
        <div
          className=""
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <h3 style={{ color: "rgb(66, 138, 33)" }}>Reset Password</h3>
        </div>
        <Grid container spacing={0}>
          {/* <div className="inputelement"> */}
          <Grid item xs={12} md={12} className="inputgrid">
            <TextField
              id="standard-basic"
              className="textfield"
              label="Old Password"
              fullWidth
              value={Name}
              onChange={(e) => setName(e.target.value)}
              style={{ color: "white" }}
            />
          </Grid>
          <Grid item xs={12} md={12} className="inputgrid">
            <TextField
              id="standard-basic"
              label="New Password"
              fullWidth
              value={Password}
              onChange={(e) => setPassword(e.target.value)}
              style={{ color: "white" }}
            />
          </Grid>

          <Grid item xs={12} md={12} className="inputgrid">
            <Button
              variant="contained"
              fullWidth
              onClick={onSubmit}
              className="buttoncolor"
            >
              Reset
            </Button>
          </Grid>
          <Grid item xs={12} md={12} className="link">
            <Button
              variant="contained"
              fullWidth
              className="buttoncolor"
              //   color="primary"
              onClick={() => history.push("/login")}
            >
              Cancel
            </Button>
          </Grid>

          {/* </div> */}
        </Grid>
      </div>
    </div>
  );
};

export default Login;
