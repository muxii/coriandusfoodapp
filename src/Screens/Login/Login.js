import React, { useEffect, useState } from "react";
import Loader from "../../utils/Loader";
import Image from "../../Assets/logo.jpeg";
import "./Login.css";
import { LoginService } from "../../utils/Services/Services";
import { useHistory } from "react-router-dom";
import {
  Button,
  FormControl,
  Grid,
  Input,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import SnackbarComponent from "../../Components/Snackbar/Snackbar";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import Cookies from "js-cookie";

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1),
  },
  textField: {
    width: "30ch",
  },
}));
const Login = () => {
  const [Name, setName] = useState("");
  const [Password, setPassword] = useState("");
  const [opensnack, setOpenSnack] = useState(false);
  const [message, setMessage] = useState("");
  const [type, setType] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const history = useHistory();
  const classes = useStyles();

  useEffect(() => {
    const cook = Cookies.get("login");
    if (cook) {
      history.push("/explore");
    }
  }, []);

  const onSubmit = () => {
    LoginService({ userName: Name, userPassword: Password })
      .then((response) => {
        if (response.data) {
          setOpenSnack(true);
          setType("success");
          setMessage("Successfully Login ");
          Cookies.set("login", true);
          Cookies.set("UserId", response.data.userId);

          sessionStorage.setItem("userData", JSON.stringify(response.data));
          setTimeout(() => {
            setOpenSnack(false);
            history.push("/explore");
          }, 1500);
        } else {
          setOpenSnack(true);
          setType("fail");
          setMessage("Failed to Login");
          setTimeout(() => {
            setOpenSnack(false);
          }, 4000);
        }
      })
      .catch((err) => {
        setOpenSnack(true);
        setType("fail");
        setMessage("Failed to Login");
        setTimeout(() => {
          setOpenSnack(false);
        }, 4000);
      });
  };

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  return (
    <>
      {opensnack && (
        <SnackbarComponent
          autoHideDuration={1500}
          open={opensnack}
          onClose={() => setOpenSnack(false)}
          isSuccess={type}
          handleClose={() => setOpenSnack(false)}
          message={message}
        />
      )}
      <div
        className="outerContainer"
        style={{ height: "100vh", backgroundColor: "black" }}
      >
        <div className="Logintext" style={{ backgroundColor: "black" }}>
          <div
            className=""
            style={{
              backgroundColor: "black",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <img src={Image} style={{ height: "150px" }} />
          </div>
        </div>

        <div className="logincont">
          <div
            className=""
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <h3 style={{ color: "rgb(66, 138, 33)" }}>
              Kindly Login to Continue
            </h3>
          </div>
          <Grid container spacing={0}>
            {/* <div className="inputelement"> */}
            <Grid item xs={12} md={12} className="inputgrid">
              <TextField
                id="standard-basic"
                className="textfield"
                label="User Name"
                fullWidth
                value={Name}
                onChange={(e) => setName(e.target.value)}
                style={{ color: "white" }}
              />
            </Grid>

            <Grid item xs={12} md={12} className="">
              <FormControl className={clsx(classes.margin, classes.textField)}>
                <InputLabel htmlFor="standard-adornment-password">
                  Password
                </InputLabel>
                {/* <Input
                  id="standard-basic"
                  label="Password"
                  fullWidth
                  // value={Password}
                  type={showPassword ? "text" : "password"}
                  onChange={(e) => setPassword(e.target.value)}
                  style={{ color: "white" }}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                      >
                        {showPassword ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  }
                /> */}
                <Input
                  id="standard-adornment-password"
                  type={showPassword ? "text" : "password"}
                  value={Password}
                  onChange={(e) => setPassword(e.target.value)}
                  // onChange={handleChange('password')}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                      >
                        {showPassword ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  }
                />
              </FormControl>
            </Grid>

            <Grid item xs={12} md={12} className="inputgrid">
              <Button
                variant="contained"
                fullWidth
                onClick={onSubmit}
                className="buttoncolor"
              >
                LOGIN
              </Button>
            </Grid>
            <Grid item xs={12} md={12} className="link">
              <Button onClick={() => history.push("/signup")}>
                <a style={{ color: "rgb(66, 138, 33)" }}>Signup</a>
              </Button>
            </Grid>
            <Grid item xs={12} md={12} className="link">
              <Button onClick={() => history.push("/reset")}>
                <a style={{ color: "rgb(66, 138, 33)" }}>Forgot Password</a>
              </Button>
            </Grid>
            {/* </div> */}
          </Grid>
        </div>
      </div>
    </>
  );
};

export default Login;
