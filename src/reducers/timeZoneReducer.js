import { TIMEZONE_EVENTS } from '../constants/actionTypes';

export const initialState = {
  loading: false,
  data: [],
  error: null,
};

const timeZonesReducer = (state, { type, payload }) => {
  switch (type) {
    case TIMEZONE_EVENTS.REQUEST:
      return { ...state, loading: true };

    case TIMEZONE_EVENTS.SUCCESS:
      return { ...state, loading: false, data: payload };

    case TIMEZONE_EVENTS.FAIL:
      return { ...state, loading: false, error: payload };

    case TIMEZONE_EVENTS.RESET:
      return initialState;

    default:
      return state;
  }
};

export default timeZonesReducer;
