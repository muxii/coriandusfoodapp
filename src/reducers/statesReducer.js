import { STATES_EVENTS } from '../constants/actionTypes';

export const initialState = {
  loading: false,
  data: [],
  error: null,
};

const statesReducer = (state, { type, payload }) => {
  switch (type) {
    case STATES_EVENTS.REQUEST:
      return { ...state, loading: true };

    case STATES_EVENTS.SUCCESS: {
      return { ...state, loading: false, data: payload };
    }

    case STATES_EVENTS.FAIL:
      return { ...state, loading: false, error: payload };

    case STATES_EVENTS.RESET:
      return initialState;

    default:
      return state;
  }
};

export default statesReducer;
