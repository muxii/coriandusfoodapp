/* eslint-disable react/prop-types */
/* eslint-disable import/no-unresolved */
/* eslint-disable global-require */
/* eslint-disable  */

export const drawerWidth = 210;

export const roles = {
  Uber: 'Uber',
  Company: 'Company',
  Client: 'Client',
};

export const a = 'a';

export const PROBLEM_PLACEHOLDER =
  'Address why this project is needed. What are the pain points? What are the symptons that have occurred because of this issue? Who is this problem affecting?';

export const SOLUTION_PLACEHOLDER =
  'This should cover a brief explanation of your project idea. Share things like what it is, where it will take place, who will be involved, ect.';

export const VISION_OF_SUCCESS_PLACEHOLDER =
  'What is the end result? Describe what a final, successfully implemented project looks like. How you know when you will have been successful with the project?';

export const IMPLEMENTATION_PLAN_PLACEHOLDER =
  'Walk through how you plan on implementing your project and insuring success. What will you do in preparation of the project? While Excecuting? After Completion? Furthermore, think about things like what supplies will it need, who needs to be involved, and how much will it cost?';

export const MEASURE_YOUR_IMPACT =
  'In addition to measuring # of people impacted and # of hours spent, every projects allows you to measure up to 3 custom metrics that you would like to improve with your project.';

export const NARRATIVE =
  'Provide an overview of what your chapter did in order to achieve this project.';

export const TRACK_IMPACT =
  'You will need to measure your metrics prior to starting the project, establish your goal for where you want the metric to be, and then re-measure the metrics once your project has been complete.';

export const CHALLENGES = 'What obstacles occurred as you began to implement your project';

export const OVERCOME_CHALLENGES = 'How did you work through the challenges in order to overcome them?'

export const PROJECT_ADVICE = 'If another group of students wanted to implement this project on their own, what advise would you give them before they start?';

export const PROJECT_IMPLEMENTATION_VIDEO = 'If you would like to help other chapters success, make aned upload a video teaching another chapter how they can succssfully implement this project on their own.';