import React, { useState, useEffect } from "react";
import clsx from "clsx";
import { makeStyles, fade } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Drawer from "@material-ui/core/Drawer";
import Box from "@material-ui/core/Box";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import Badge from "@material-ui/core/Badge";
import MenuIcon from "@material-ui/icons/Menu";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Popper from "@material-ui/core/Popper";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import CloseIcon from "@material-ui/icons/Close";
import NewList from "../List/NewList";
import { useStyles } from "./css";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import { useHistory } from "react-router-dom";
import axios from "axios";
import SmallImage from "../../../Assets/smalllogo.jpeg";
import { LoginContext } from "../../../contexts/loginContext";
import { APPBAR_EVENTS } from "../../../constants/actionTypes";

export default function AppBar1(props) {
  // console.log(props);
  const classes = useStyles();
  const anchorRef = React.useRef(null);
  const [open, setOpen] = React.useState(false);
  const [open1, setOpen1] = React.useState(false);

  // const [redirect, setRedirect] = React.useState(false);
  const [redirect, setRedirect] = useState(false);
  const [loginState, loginDispatch] = React.useContext(LoginContext);

  const [data, setData] = useState(sessionStorage.getItem("payload"));
  const payload = JSON.parse(data);
  const history = useHistory();
  // console.log(payload);

  useEffect(() => {
    // if (sessionStorage.getItem("merchantId")) {
    //   setRedirect(false);
    // } else {
    //   setRedirect(true);
    // }
    console.log(props);
    console.log(loginState.data);
    loginDispatch({
      type: APPBAR_EVENTS.SUCCESS,
      payload: "Hello",
    });
    console.log("Hello Here!!");
  }, []);

  const [locationNames, setLocationNames] = useState("");

  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handleToggle = () => {
    setOpen1((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen1(false);
  };

  function handleListKeyDown(event) {
    if (event.key === "Tab") {
      event.preventDefault();
      setOpen1(false);
    }
  }

  //  return focus to the button when we transitioned from !open -> open
  const prevOpen = React.useRef(open1);
  React.useEffect(() => {
    if (prevOpen.current === true && open1 === false) {
      anchorRef.current.focus();
    }

    prevOpen.current = open1;
  }, [open1]);

  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

  // if (redirect) {
  //   return <Redirect to="login" />;
  // }

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        style={{
          color: "#fff",
          backgroundColor: "black",
          boxShadow: "5px 0px #D63F27",
        }}
        position="fixed"
        className={clsx(classes.appBar, open && classes.appBarShift)}
      >
        <Toolbar className={classes.toolbar}>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            className={clsx(
              classes.menuButton,
              open && classes.menuButtonHidden
            )}
          >
            <MenuIcon style={{ color: "white" }} />
          </IconButton>

          {!open && (
            <>
              <Typography
                component="h1"
                variant="h6"
                color="inherit"
                noWrap
                className={classes.title}
              >
                <div
                  className=""
                  style={{ display: "flex", justifyContent: "space-between" }}
                >
                  <div className="">
                    <div className="text">
                      <div style={{ padding: "5px" }}>Explore Menu</div>
                    </div>
                  </div>
                </div>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "flex-end",
                    alignItems: "center",
                    padding: "20px",
                  }}
                >
                  <Badge
                    badgeContent={props.menuItemCount && props.menuItemCount}
                    color="secondary"
                    style={{ zIndex: 1000 }}
                    onClick={() => history.push("/viewCart")}
                  >
                    <ShoppingCartIcon style={{ color: "white" }} />
                  </Badge>
                </div>
              </Typography>
            </>
          )}
          {/* profile */}
          <div></div>

          {/* end */}
        </Toolbar>
      </AppBar>
      <Drawer
        className={classes.drawer}
        // variant="temporary"
        // style={{ backgroundColor: "black" }}
        variant="temporary"
        onEscapeKeyDown={handleDrawerClose}
        onBackdropClick={handleDrawerClose}
        classes={{
          paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
        }}
        open={open}
      >
        <div
          className={classes.toolbarIcon}
          style={{
            backgroundColor: "black",
            color: "white",
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          {/* <h4 className={classes.toolbarIcon}>Coriandis</h4> */}
          <img src={SmallImage} className={classes.imagesize} />
          <IconButton onClick={handleDrawerClose}>
            <CloseIcon style={{ color: "white" }} />
          </IconButton>
        </div>
        <NewList />
      </Drawer>
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container
          maxWidth="lg"
          className={classes.container}
          style={{ backgroundColor: "black" }}
        >
          {props.children}
        </Container>
      </main>
    </div>
  );
}
