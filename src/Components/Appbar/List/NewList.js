import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import ListSubheader from "@material-ui/core/ListSubheader";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import HomeIcon from "@material-ui/icons/Home";
import ListAltIcon from "@material-ui/icons/ListAlt";
import StoreIcon from "@material-ui/icons/Store";
import AssignmentIcon from "@material-ui/icons/Assignment";
import { Link, useHistory, withRouter } from "react-router-dom";
import { useStyles } from "./css";
import { Tooltip } from "@material-ui/core";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import DescriptionIcon from "@material-ui/icons/Description";
import { AccountCircleRounded } from "@material-ui/icons";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import PowerSettingsNewIcon from "@material-ui/icons/PowerSettingsNew";
import Cookies from "js-cookie";

const isActive = (history, path) => {
  if (history.location.pathname === path)
    return {
      width: "100%",
      color: "white",
      backgroundColor: "black",
    };
  else
    return {
      width: "100%",
      color: "white",
      backgroundColor: "black",
    };
};

const NestedList = ({ history }) => {
  // console.log(history);
  const classes = useStyles();
  const history1 = useHistory();
  const logout = () => {
    Cookies.remove("login");
    history1.push("/login");
  };

  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      subheader={
        <ListSubheader
          component="div"
          id="nested-list-subheader"
        ></ListSubheader>
      }
      className={classes.root}
    >
      {/*List start */}
      <Link style={{ textDecoration: "none" }} to="/explore">
        <ListItem button style={isActive(history, "/explore")}>
          <Tooltip title="Upload Document">
            <ListItemIcon>
              <AccountCircleIcon className={classes.imgIcon} />
            </ListItemIcon>
          </Tooltip>
          <ListItemText primary="Profile" />
        </ListItem>
      </Link>

      <Link style={{ textDecoration: "none" }} to="/orderLists">
        <ListItem button style={isActive(history, "/orderLists")}>
          <Tooltip title="Document Repository">
            <ListItemIcon>
              <ListAltIcon className={classes.imgIcon} />
            </ListItemIcon>
          </Tooltip>
          <ListItemText primary="Orders" />
        </ListItem>
      </Link>

      <Link style={{ textDecoration: "none" }}>
        <ListItem
          button
          style={isActive(history, "/explore")}
          onClick={
            () => logout()
            // return {
            //   Cookies.remove("login")

            // }
          }
        >
          <Tooltip title="Aggregrate Document Repository">
            <ListItemIcon>
              <PowerSettingsNewIcon className={classes.imgIcon} />
            </ListItemIcon>
          </Tooltip>
          <ListItemText primary="Log Out" />
        </ListItem>
      </Link>

      {/* <Link style={{ textDecoration: "none" }} to="/return">
        <ListItem button style={isActive(history, "/return")}>
          <ListItemIcon>
            <KeyboardReturnIcon className={classes.imgIcon} />
          </ListItemIcon>
          <ListItemText primary="Return & Refund" />
        </ListItem>
      </Link>
      <Link style={{ textDecoration: "none" }} to="#">
        <ListItem button style={isActive(history, "#")}>
          <ListItemIcon>
            <AccountBalanceWalletIcon className={classes.imgIcon} />
          </ListItemIcon>
          <ListItemText primary="Accounts" />
        </ListItem>
      </Link> */}
    </List>
  );
};

export default withRouter(NestedList);
