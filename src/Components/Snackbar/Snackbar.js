/* eslint-disable  */
import React from "react";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { makeStyles } from "@material-ui/core/styles";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
}));

const SnackbarComponent = (props) => {
  const classes = useStyles();
  console.log(props);
  return (
    <Snackbar
      open={props.open}
      style={props.style && props.style}
      // anchorOrigin={props.anchorOrigin && props.anchorOrigin}
      // autoHideDuration={2000}
      autoHideDuration={props.autoHideDuration}
      onClose={props.onClose}
    >
      {props.isSuccess === "success" ? (
        <Alert onClose={props.handleClose} severity="success">
          {props.message}
        </Alert>
      ) : (
        props.isSuccess === "fail" && (
          <Alert onClose={props.handleClose} severity="error">
            {props.message}
          </Alert>
        )
      )}
    </Snackbar>
  );
};

export default SnackbarComponent;
