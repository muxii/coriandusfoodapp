import React, { useState } from "react";
import PropTypes from "prop-types";
import { makeStyles, rgbToHex } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import PhoneIcon from "@material-ui/icons/Phone";
import FavoriteIcon from "@material-ui/icons/Favorite";
import PersonPinIcon from "@material-ui/icons/PersonPin";
import HelpIcon from "@material-ui/icons/Help";
import ShoppingBasket from "@material-ui/icons/ShoppingBasket";
import ThumbDown from "@material-ui/icons/ThumbDown";
import ThumbUp from "@material-ui/icons/ThumbUp";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import "./Menu.css";
import { LocalDining } from "@material-ui/icons";
import {
  Button,
  CircularProgress,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
} from "@material-ui/core";

import Image from "../../Assets/comingSoon.jpg";
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-prevent-tabpanel-${index}`}
      aria-labelledby={`scrollable-prevent-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-prevent-tab-${index}`,
    "aria-controls": `scrollable-prevent-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: "black",
    borderBottomColor: "2px solid gray",
  },
}));

const useStyles1 = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    color: "white",
  },
  select: {
    borderBottom: "3px solid white",
    borderRadius: "5%",
    color: "white",
    fontSize: "12px",
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));
export default function ScrollableTabsButtonPrevent(props) {
  const classes = useStyles();
  const classes1 = useStyles1();
  const [value, setValue] = useState(props.value !== "" ? props.value : 0);
  const [show, setShow] = useState(false);
  const [id, setId] = useState("");
  const [addId, setAddId] = useState("");

  console.log(props);
  console.log(value);

  const showing = (id) => {
    console.log(id);
    setShow(!show);
    setId(id);
  };

  const handleChangeselect = (event, val) => {
    console.log(val);
  };
  const handleChange = (event, newValue) => {
    setValue(newValue);

    console.log(newValue);
  };
  const showAddButton = (id) => {
    setAddId(id);
    props.showAddButton(id);
  };
  return (
    <div className={classes.root}>
      <AppBar
        style={{
          position: "fixed",
          marginTop: "55px",
          zIndex: 1000,
          borderBottomColor: "2px solid gray",
        }}
      >
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons="off"
          aria-label="scrollable prevent tabs example"
          style={{
            // backgroundColor: "rgb(66, 138, 33)",
            backgroundColor: "black",
            borderBottomColor: "2px solid gray",
          }}
        >
          {props.menu &&
            props.menu.map((results) => {
              //   console.log(results);
              return (
                <Tab
                  label={results.menueName}
                  aria-label="phone"
                  {...a11yProps(props.value !== "" ? props.value : 0)}
                  onClick={() => props.setValue(results.menueId)}
                />
              );
            })}
        </Tabs>
      </AppBar>

      {props.menuItem && props.menuItem
        ? props.menuItem.map((results, index) => {
            return (
              <TabPanel
                className="maincont"
                value={props.value}
                index={props.value !== "" ? props.value : 0}
                key={index}
              >
                <Grid
                  container
                  direction="row"
                  justify="center"
                  alignItems="center"
                >
                  <img src={results.imageName} className="image-size" />
                </Grid>
                <Grid
                  container
                  direction="row"
                  justify="start"
                  alignItems="center"
                >
                  <div style={{ fontFamily: "Poor Richard", fontSize: "20px" }}>
                    &#x20B9;{results.price}
                  </div>
                </Grid>
                <Grid
                  container
                  direction="row"
                  justify="center"
                  alignItems="center"
                >
                  <div style={{ fontFamily: "Poor Richard", fontSize: "20px" }}>
                    {results.itemName}
                  </div>
                </Grid>
                <Grid direction="row" justify="center" alignItems="center">
                  <div style={{ fontFamily: "Poor Richard", fontSize: "18px" }}>
                    {results.description}
                  </div>
                </Grid>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    padding: "10px",
                  }}
                >
                  <div>
                    <Button
                      variant="contained"
                      onClick={() => showing(results.menueItemsId)}
                      color="primary"
                      key={index}
                    >
                      Customize
                    </Button>
                  </div>

                  <div>
                    <Button
                      variant="contained"
                      onClick={() => showAddButton(results.menueItemsId)}
                      color="primary"
                    >
                      Add to Cart +
                    </Button>
                  </div>
                </div>
                {show && id == results.menueItemsId && (
                  <Grid
                    key={index}
                    container
                    direction="row"
                    justify="space-between"
                  >
                    {props.pizzaDrop &&
                      props.pizzaDrop.map((res) => {
                        return (
                          <FormControl className={classes1.formControl}>
                            <InputLabel
                              id="demo-simple-select-label"
                              style={{ color: "white" }}
                            >
                              {res.varientCatagoryName}
                            </InputLabel>
                            <Select
                              labelId="demo-simple-select-label"
                              id="demo-simple-select"
                              // value={age}
                              onChange={(e) => handleChangeselect(e, value)}
                              className={classes1.select}
                              //   style={{ color: "white", backgroundColor: "#3F51B5" }}
                            >
                              {res &&
                                res.foodDeliveryMenuItemsVarientCatagoryItemsDTOList.foodDeliveryMenuItemsVarientCatagoryItemsDTOList.map(
                                  (catResponse, i) => {
                                    return (
                                      <MenuItem
                                        key={i}
                                        style={{ fontSize: "12px" }}
                                        value={
                                          catResponse.menueVariantCatagoryId
                                        }
                                      >
                                        {catResponse.name +
                                          " - " +
                                          catResponse.addOnPrice}
                                        &nbsp;&#x20B9;
                                      </MenuItem>
                                    );
                                  }
                                )}
                            </Select>
                          </FormControl>
                        );
                      })}
                  </Grid>
                )}
              </TabPanel>
            );
          })
        : props.menuItem == null && (
            <TabPanel className="" value={props.value} index={props.value}>
              <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
              >
                {
                  <img src={Image} style={{ width: "100%" }} />
                  // <CircularProgress disableShrink style={{ color: "white" }} />
                }
              </Grid>
            </TabPanel>
          )}
    </div>
  );
}
